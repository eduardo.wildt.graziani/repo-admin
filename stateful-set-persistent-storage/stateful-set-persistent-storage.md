# Storage

## EmptyDir - sometimes ephemeral

First let's look at the deployment defined in 'deployment-emptyDir.yaml'. There is an `initContainer`, that runs before
the main container and creates a touches with on a shared `emptyDir` volume.
```
      initContainers:
        - name: init-hello
          image: busybox:1.28
          volumeMounts:
            - mountPath: /cache
              name: cache-volume
          args:
            - /bin/sh
            - -c
            - touch /cache/"$(date +%s)"-hello-from-initcontainer;
```

There is also main container, which also creates an empty file using `touch`

```
         ...
          volumeMounts:
            - mountPath: /cache
              name: cache-volume
          args:
            - /bin/sh
            - -c
            - ls /cache/; touch /cache/"$(date +%s)";
```

---
**Question**

What do you think will be the output of `ls /cache` (You can get the output using `kubectl logs deploy`?

---

Let's first begin watching the pods in another terminal window

```
$ kubectl get pods -w
```

Next, deploy the file `deployment-emptyDir.yaml`:

```
$ kubectl apply -f deployment-emptyDir.yaml
deployment.apps/empty-dir-demo created
```

---
**Question**

Why is the pod in a crashloop? (No need to fix it)

---

Look at the pods logs for a history of which containers where started.
```
$ kubectl logs deploy/empty-dir-demo
```

Now delete the first pod of the deployment:

```
$ kubectl delete pod $(kubectl get pod -l app=empty-dir-demo -o jsonpath="{.items[0].metadata.name}")
pod "empty-dir-demo-5d8cfb59d8-gtgq9" deleted
```

And look at the logs again:
```
$ kubectl logs deploy/empty-dir-demo
```

Can you explain what happened? Why did the output continue to grow although the pod was crashlooping? Why was the history lost after we deleting the pod and a new one was spawned?


## Cleanup
```
$ kubectl delete deploy/empty-dir-demo
```

# PersistentVolumes

Lets first list the available storage classes:

```
$ kubectl get storageclass
NAME                PROVISIONER                RECLAIMPOLICY   VOLUMEBINDINGMODE   ALLOWVOLUMEEXPANSION   AGE
azurefile           kubernetes.io/azure-file   Delete          Immediate           true                   25h
azurefile-premium   kubernetes.io/azure-file   Delete          Immediate           true                   25h
default (default)   kubernetes.io/azure-disk   Delete          Immediate           true                   25h
managed-premium     kubernetes.io/azure-disk   Delete          Immediate           true                   25h
```

Usually, PersistentVolumes are created indirectly using a `PersistentVolumesClaim`. Let's create one:
```
$ kubectl apply -f pvc-test.yaml
persistentvolumeclaim/pvc-test created
```

First, the `PersistentVolumeClaim` will be in status `Pending`.

```
$ kubectl get persistentvolumeclaim
NAME                   STATUS    VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
pvc-test               Pending                                                                        default        1s
pvc-test               Bound     pvc-c15f23ba-d9fc-4dd1-9013-507d7a554b82   1Gi        RWO            default        42s
```

Why is it `Pending`? 

```
$ kubectl describe pvc pvc-test 
Events:
  Type    Reason                Age               From                         Message
  ----    ------                ----              ----                         -------
  Normal  WaitForFirstConsumer  7s (x3 over 32s)  persistentvolume-controller  waiting for first consumer to be created before binding
```

Lets try to create a consumer
```
$ kubectl apply -f deployment-pvc.yaml
deployment.apps/pvc-demo created
```

Then look at the PersistentVolumeClaim again

```
$ kubectl get pvc
NAME       STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
pvc-test   Bound    pvc-fc6bb25e-4c18-4c3d-92db-05b8df13b7a1   1Gi        RWO            default        71s
```
$  kubectl get persistentvolume
```
$ kubectl get pv
NAME                                       CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM                          STORAGECLASS   REASON   AGE
pvc-c15f23ba-d9fc-4dd1-9013-507d7a554b82   1Gi        RWO            Delete           Bound    default/pvc-test               default                 4m49s
```
We can also look at the associated `PersistentVolumes` (_the resource isn't namespaces, so you will see all `pv`s):

---
**Question**

What do you guess will happen, after we delete the `PersistentVolumeClaim`?
And what do you guess will happen, after we delete the `Deployment`?

---

First, watch the `PersistentVolumes` in a separate terminal:
```
$  kubectl get persistentvolume
```



Let's see what happens when we pods the volume is mounted in:
```
$ kubectl delete -f deployment-pvc.yaml
deployment.apps "pvc-demo" deleted
```

```
$ kubectl delete -f pvc-test.yaml
persistentvolumeclaim "pvc-test" deleted
```

Now check if the PersistentVolumes is still there (this may take a few seconds) 
```
$ kubectl get persistentvolume
```

Recreate the `PersistentVolumeClaim`.
```
$ kubectl apply -f ./pvc-test.yaml
persistentvolumeclaim/pvc-test created
$ kubectl apply -f deployment-pvc.yaml
deployment.apps/pvc-demo created
```

But this time, let's make sure out data doesn't get lost after we remove the PersistenVolumeClaim: _Please take a moment and try to understand the patching command, since this can sometimes be useful_
```
$ kubectl patch pv $(kubectl get pvc pvc-test -o jsonpath="{.spec.volumeName}") -p '{"spec":{"persistentVolumeReclaimPolicy":"Retain"}}'
persistentvolume/pvc-55cfeef7-3bbf-484c-8473-543e5b22942c patched
```

The `ReclaimPolicy` is now set to `Retain`.
```
$  kubectl get persistentvolume
NAME                                       CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM                          STORAGECLASS   REASON   AGE
pvc-55cfeef7-3bbf-484c-8473-543e5b22942c   1Gi        RWO            Retain           Bound    default/pvc-test               default                 2m53s
```

Let's delete the `PersistentVolumeClaim` and it's consumer again.
kubectl delete -f deployment-pvc.yaml
```
$ kubectl delete -f deployment-pvc.yaml
deployment.apps "pvc-demo" deleted
$ kubectl delete -f ./pvc-test.yaml
persistentvolumeclaim "pvc-test" deleted
```

The `PersistentVolume` is now `Released` but not deleted:
```
$  kubectl get persistentvolume
NAME                                       CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS     CLAIM                          STORAGECLASS   REASON   AGE
pvc-55cfeef7-3bbf-484c-8473-543e5b22942c   1Gi        RWO            Retain           Released   default/pvc-test               default                 4m23s
```

When we create the `PersistentVolumeClaim` again, it will be associated with it again.
```
$ kubectl apply -f pvc-test.yaml
persistentvolumeclaim/pvc-test created
$ kubectl apply -f deployment-pvc.yaml
deployment.apps/pvc-demo created
```

Once it has crashed at least once, delete a pod again and look at the output:
```
$ kubectl delete pod $(kubectl get pod -l app=pvc-demo -o jsonpath="{.items[0].metadata.name}")
  pod "pvc-demo-7899dcc6c6-bh4s9" deleted
```

Look at the newly created pods output:
```
$ kubectl logs deploy/pvc-demo
```
---
**Question**

Observe (using `describe` and `get` etc) when we scale the deployment? How do you explain and how would you fix the resources?

```
$ kubectl scale deploy/pvc-demo --replicas 2
```

## Cronjob

Delete the deployment we created with `kubectl delete -f deployment-pvc.yaml`, since the cronjob example we will try out next uses the same pvc.
Start by deploying the cronjob:

```bash
$ kubectl apply -f cronjob-pvc.yaml
```
Watch the pods an jobs in another shell (end the background processes later with `killall kubectl`)

```bash
$ kubectl get pods -w & kubectl get jobs -w
```

### Exercise 1

- Modify the cronjob to only run on the 11th of November at 11:11:11.


---


## Stateful Set

Maybe the first exercise helped you understand the reason for the existance of `StatefulSets` Before deploying a stateful set, start watching the pods in your namespace in another shell as we have done many times before:

```bash
kubectl get pods -w
```

Deploy the statefull set in stateful-set.yaml

```bash
kubectl apply -f stateful-set.yaml
```

Observe how `PersistentVolumes` and `PersistenVolumeClaims` where created implicitly:

```bash
kubectl get pvc
kubectl get pv
```

Port forward to the headless service and try to get a response
```
kubectl port-forward svc/nginx 8080:80
```

Open a new shell
```bash
curl localhost:8080
```

The result is unsatisfactory, so lets create a `index.html`

```bash
kubectl get pods -o name | grep web| while read -r pod; do kubectl exec $pod -- /bin/bash -c "echo 'Enjoy your life in full trains.' > /usr/share/nginx/html/index.html"; done
```

Let's scale the stateful set back to 0 and then scale it up again (keep watching what happens with `kubectl get pods -w`)

```bash
kubectl scale statefulset web --replicas 0
kubectl scale sts web --replicas 3
```

Do you think out change to the index.html was persisted?
```bash
curl localhost:8080
```

Observe `pv`s and `pvcs` again
```bash
kubectl get pvc
kubectl get pv 
```



