## Kustomize 

As an already included basic configuration management tool, kustomize can be used with kubernetes from the get go.

The directory has the following layout. 

```
.
├── bases
│   └── echo
│       ├── deployment.yaml
│       └── kustomization.yaml
├── kustomization.yaml
├── kustomize-basics.md
└── patch.yaml
```

We have an outer `kustomization.yaml` as an entry point for all configuration. The `bases` directory serves as a space for all applicaton specific deployment configurations.


### Kustomization.yaml 

We have a very simple `kustomization.yaml` prepared, that we can use to elaborate the basic functionality of kustomize. 


```yaml
# kustomization.yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

bases:
- bases/echo                            # applications we want deploy

images:
- name: echoserver                      # name mapping to match name in deployment.yaml
  newName: k8s.gcr.io/echoserver        # fully qualified registry/name 
  newTag: "1.4"                         # target tag 

configMapGenerator:
- name: kustomize-basics                # config map name that we want to generate
  files:
  - kustomize-basics.md                 # files we want to inject into config map

patchesStrategicmerge:
- patch.yaml                            # patches we want to apply
```

We define which bases to use and qualify the full image names in our kustomizaion file "from the outside" instead of the deployment directly. This way we can update the image names and versions across many different deployments with ease.

Additionaly we generate a `configmap` with the generator. We inject the very readme you are currently reading into this configmap.

As an additonal example we apply a small patch.


```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: echo
spec:
  replicas: 1
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
```

The patch applies a rolling update strategy to the echo deployment and sets the number of replicas that should be used. With patches, reusable infrastructure code can be applied to many different deployments, respecting DRY.



### Kustomize Basics


As promised earlier kustomize is already bultin. Let's look at the configuration we are going to apply.

```bash
kubectl kustomize
```
The output shows that all our Kustomizations to the basic deployment are applied.
So let's deploy.

```bash
kubectl apply -k .
```

Inspect what we have created:

```bash
kubectl get cm,deployments,pods
```

### Exercise

- Find this very readme inside of the pod we just deployed
