#!/usr/bin/env sh
if test -f /app/passwords.txt; then
    echo "passwords.txt should not be contained in docker image!"
else
    echo "Enjoy life in full trains.";
fi
