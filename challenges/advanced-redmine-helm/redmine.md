# Challenge - Create a helm chart for redmine

Objective:
* Create a `helm` chart for [redmine](https://hub.docker.com/_/redmine) from scratch

Requirements:
* Make the data persistant using a persistant volume claim (`/usr/src/redmine/files`)
* Make redmine accessible using an ingress

Optional
* Use the database you created in challenge 01 (`REDMINE_DB_NAME`) or the `bitname/mariadb` chart

