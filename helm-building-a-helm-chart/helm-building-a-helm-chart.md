# Helm (2/2) Building you a helm chart

[[_TOC_]]

In this exercise, we will try to build a helm chart for a prebuilt spring boot application serving a static site.

**Note:** This is one if not perhaps the most challenging exercise in this workshop. Not all necessary steps will be explained in detail, so you may have to use the techniques and commands we learned in previous exercises. Don't feel bad if you don't finish.

## Useful helm commands for creating and debugging helm charts

|  helm command | description   |
|---|---|
|  `helm create [name]` |  Create a directory containing the common chart files and directories (`chart.yaml`, `values.yaml`, `charts/` and `templates/`): |
|  `helm package [chart-path]` |  Package a chart into a chart archive |
|  `helm lint [chart] ` |  Run tests to examine a chart and identify possible issues |
|  `helm show all [chart]` |   Inspect a chart and list its default values |
|  `helm show values [chart] ` | Display the chart’s values |
|  `helm template [release] [chart] (-f/--values [values-file]) (-s/--show-only [template file]` | Render one or all manifests of a helm chart (e.g.: `helm template test . -f values-edited.yaml -s templates/ingress.yaml`)  |
|  `helm upgrade [release] [chart] --install (-n/--namespace [namespace])` | Upgrade a release. If it does not exist on the system, install it |
|  `helm get manifest [release]` |  display the manifest for a named release currently deployed |
|  `helm get values [release] ` |   display the values used to deploy a release currently deployed |


## Creating a new helm chart from scratch

To create a helm chart, we don't have to start from scratch or copy from other sources. Helm comes packaged with a helm template.
Since we are going to deploy a container image containing spring boot, `spring-demo-chart` seems to be a fitting name.
To generate a helm chart based on this template, type:

```bash
helm create spring-demo-chart
```
Helm will create a new directory `spring-demo-chart` containing an amount of files that may seem frightening at first.

```
spring-demo-chart/
├── Chart.yaml
├── charts
├── templates
│   ├── NOTES.txt
│   ├── _helpers.tpl
│   ├── deployment.yaml
│   ├── hpa.yaml
│   ├── ingress.yaml
│   ├── service.yaml
│   ├── serviceaccount.yaml
│   └── tests
│       └── test-connection.yaml
└── values.yaml
```

## Exploring and deploying a local helm chart

Try out the command `helm show values` with your current helm chart. This command will output the contents of `values.yaml`

```
helm show values .
```
**Note:** This command is useful mainly for helm charts from remote repositories e.g.: `helm show values bitnami/nginx`

You will note some mentions of some known concepts but also of some advanced concepts like `securityContext` or `nodeAffinity`. You can safely ignore those at the moment.


As we did before, we will start by deploying the sample helm chart as is...

```
helm upgrade --install spring-demo-release ./spring-demo-chart
```

After a successful deployment some information and instructions will be shown. Please the output in mind when you explore the helm release, since we will soon begin with influencing this output.

Confirm your release was successful:

```
helm ls
```

### Exercise 1
  1. Something apart from our well known basic building blocks `deployment,service`, something else was deployed. Can you find out using `helm get` commands?
  2. What is the origin of `helm status`? 
     1. `REVISION`
     2. `NOTES`
  3. Edit your charts `NOTES.txt`, so it contains at least your name. You can safely ignore all those curly braces. 
  4. The output of `helm ls` shows a *chart version* and *app-version*. Change those versions so that `chart version` is `0.2.0` and `app-version` is `1.17.0`
  5. Edit the ImagePullPolicy to `Always` in `values.yaml`. This is often regarded as a best practice.   
  5. Deploy a new revision of your chart and confirm your changes where successful
   
### Bonus Exercise 1
 1. Use the go templating mechanism to insert your name from the `values.yaml` for this add a field there and include it in `NOTES.txt` using `{{ .Values }}`
 2. Add `bitnami/redis` as a chart dependency:
  ```
  # Chart yaml 
  - name: redis
    condition: redis.enabled
    version: "16.3.1"
    repository: "https://charts.bitnami.com/bitnami"
  ```
 3. You will notice, that updating again will result in an error. Fetch the dependent chart using `helm dependency update`
 4. Disable the redis chart by default by adding the value `redis.enabled: false` *(in separate lines)*
   
---

## Modifying the chart template

The container image we currently deploy is `nginx:latest`. This is obviously not spring boot. 

Create a values file for your custom values referencing another image. Think about separating default values from
environment or release specific values.

```shell
cat <<EOT >> values-dev.yaml
image:
  repository: registry.gitlab.com/mt-ag-k8s-training-course/kubernetesexercises/spring-boot-demo
  pullPolicy: Always
  tag: "1.18.0"
EOT
```

Alternatively, you could try using the image you built during the `gitlab-ci` exercise.

Now update your helm release
```
helm upgrade --install spring-demo-release ./spring-demo-chart -f values-dev.yaml
```

You can find the user supplied values again with a command that is very useful for debugging helm releases
```shell
helm get values spring-demo-release
```


### Exercise 1

1. The `release` doesn't seem to have been deployed correctly. Try to diagnose the problem using `kubectl logs`, `kubectl describe` and `kubectl get`. Be especially mindful of ports. Why is the pod getting restarted?

**Note**

[Troubleshooting kubernetes](https://learnk8s.io/troubleshooting-deployments) is a flowchart providing a good overview about how to troubleshoot errors like this.


2. Edit the helm chart using the insight you gained. Deploy your changes.
   _Spoiler:_
   ```
   echo YGBgc2hlbGwKICAgIyB0ZW1wbGF0ZXMvZGVwbG95bWVudC55YW1sCiAgICAgIGNvbnRhaW5lcnM6CiAgICAgICAgLSBuYW1lOiB7eyAuQ2hhcnQuTmFtZSB9fQogICAgICAgICAgLi4uCiAgICAgICAgICBwb3J0czoKICAgICAgICAgICAgLSBuYW1lOiBodHRwCiAgICAgICAgICAgICAgY29udGFpbmVyUG9ydDogODAgIzw9PSBDaGFuZ2UgdG8gODA4MApgYGA=  \
     | base64 -d
    ```
2. Try to access the application using the command shown in `helm status` / `NOTES`

### Exercise 2
1. Set some `requests` and `limits` as a default in `values.yaml`. You can use `limits.cpu: "1.0" and limits.memory:"512Mi"`. Decide about the right `requests.cpu` and `requests.memory` using `kubectl top pods`.
2. Calling `index.html` is understandably not the best way to facilitate liveness and readyness probes. Luckily, `spring boot actuator` was enabled when building this image. Switch the livenessProbes and readinessProbes from `/` to `/actuator/health/liveness` and `/actuator/health/readiness`.
    4.1 (optional) Instead of hardcoding the liveness and readiness Paths, make those values available for configuration in `values.yaml`
    4.2. (optional) Add a startupProbe

### Bonus Exercise 2

1. Make the application accessible by configuring the ingress correctly. (in `values-dev.yaml`)
2. Enable tls with a valid certificate for this ingress 

_Spoiler_:
```shell
echo aW5ncmVzczoKICBlbmFibGVkOiB0cnVlCiAgY2xhc3NOYW1lOiAibmdpbngiCiAgYW5ub3RhdGlvbnM6CiAgICBjZXJ0LW1hbmFnZXIuaW8vY2x1c3Rlci1pc3N1ZXI6IGxldHNlbmNyeXB0CiAgICBrdWJlcm5ldGVzLmlvL3Rscy1hY21lOiAidHJ1ZSIKICBob3N0czoKICAgIC0gaG9zdDogY2hhbmdlbWUhLm10YWctazhzLXNjaHVsdW5nLnNpdGUKICAgICAgcGF0aHM6CiAgICAgICAgLSBwYXRoOiAvCiAgICAgICAgICBwYXRoVHlwZTogUHJlZml4CiAgdGxzOgogICAgLSBzZWNyZXROYW1lOiBjaGFuZ2VtZS1tdGFnLWs4cy1zY2h1bHVuZy1zaXRlCiAgICAgIGhvc3RzOgogICAgICAgICAtIGNoYW5nZW1lIS5tdGFnLWs4cy1zY2h1bHVuZy5zaXRlCg== | base64 -d
```

### Bonus Exercise 3

1. The spring boot application is configured to output the contents of the property 'greeting' at `/greeting`.
2. Decide on a new greeting and pass it to the application using an environment variable called `GREETING`. The contents of this environment variable should not be hardcoded.
3. Create a configmap inside your helm chart. Set the `GREETING` variable there and  pass it's content to the container.
    ```shell
    envFrom:
        - configMapRef:
                  name: {{ include "spring-demo-chart.fullname" . }}
    ```
    ---
