# Services

Let's start by creating an arbitrary deployment and our trusty debug pod:

```
$ kubectl create deployment hello --image=gcr.io/google-samples/hello-app:1.0 --replicas 4
$ kubectl create deployment debug --image=praqma/network-multitool
```

---
**Note**: praqma/network-multitool is a container image containing lots of tools you might consider basic,
but are in fact often rightfully missing from docker images. (e.g. wget, curl, nslookup, tcpdump, traceroute,
 vi etc.)
---

## ClusterIP Service

Expose the deployment as a ClusterIP service

```
$ kubectl expose deployment hello --port 80 --target-port=8080 --type ClusterIP
```

Look at what was created
```
$ kubectl get svc

NAME      TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)   AGE
hello     ClusterIP   10.0.89.176   <none>        8080/TCP    2m10s
```

Now open a shell on the pod debug pod

```
$ kubectl exec --stdin --tty deployment/debug -- /bin/sh
bash-5.0# 
```

---
**Note**: 
We used `svc` as abbrevation for `service`. You can list all available resource types and their abbrevations with 
`kubectl api-resources`
---

Try to `curl` the `ClusterIP` of the service, we created previously

```bash

$ curl -s 10.0.89.176

Hello, world!
Version: 1.0.0
Hostname: web-557f59c6cf-v54ds
```

Now try to call the service using DNS **(insert the name of your namespace (you can see the current contexts namespace with `kubectl config get-contexts`)**.
bash
```
$ curl hello.NAMESPACE.svc.cluster.local
Hello, world!
Version: 1.0.0
Hostname: web-557f59c6cf-v54ds
```

Log out of the shell using `exit` or with `CTRL-D`


## NodePort Service



Let's try out the NodePort Servicetype next. Create a nodeport service using a manifest.

```bash
$ kubectl apply -f nodeport.yaml 
```

```
$ kubectl get svc
NAME             TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)        AGE
hello            ClusterIP   10.0.89.176   <none>        80/TCP         78m
hello-nodeport   NodePort    10.0.36.207   <none>        80:30007/TCP   89s
```

---
 **Note**: 
Of course only one Service per Cluster can claim the Port 30007
---

We now have a cluster-wide port for this deployment on all worker nodes. Knowing the public ip of our nodes, we can directly
communicate with it via this pod from outside the cluster. Though those nodes didn't get external ips because we solved the problem of communication with those nodes differently.

---
 **Note**: 
Due to peculiarities with aks we have to temporarily switch to our admin kubeconfig.
---

```bash
kubectl get nodes -o wide
NAME                                STATUS   ROLES   AGE    VERSION   INTERNAL-IP   EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION     CONTAINER-RUNTIME
aks-agentpool-22444322-vmss000000   Ready    agent   2d5h   v1.21.7   10.240.0.4    <none>        Ubuntu 18.04.6 LTS   5.4.0-1064-azure   containerd://1.4.9+azure
aks-agentpool-22444322-vmss000001   Ready    agent   2d5h   v1.21.7   10.240.0.5    <none>        Ubuntu 18.04.6 LTS   5.4.0-1064-azure   containerd://1.4.9+azure
aks-agentpool-22444322-vmss000002   Ready    agent   2d5h   v1.21.7   10.240.0.6    <none>        Ubuntu 18.04.6 LTS   5.4.0-1064-azure   containerd://1.4.9+azure
```

Since those nodes don't have and EXTERNAL-IP, you have to use the INTERNAL-IP instead. Instead open a shell on your `debug` container inside your cluster `kubectl run debug --rm -it --image=praqma/network-multitool -- /bin/bash`. Those internal IPs are not IPs inside the virtual kubernetes network but the azure vnet.

```bash
$ curl -s <NODE_INTERAL_IP>:30007

Hello, world!
Version: 1.0.0
Hostname: web-557f59c6cf-v54ds
```

You should try all the other node ips as well.

## LoadBalancer Service

---
 
 **Note**: 

There is a limit of public ip addresses allowed for each azure account, so there is a possibility that not everybody will be able to succeed in this exercise.

---
 
Now there is the final service type to communicate with a pod, the LoadBalancer Service.
Some cloud providers allow you to specify the loadBalancerIP. In those cases, the load-balancer is created with the user-specified loadBalancerIP. If the loadBalancerIP field is not specified, the loadBalancer is set up with an ephemeral IP address. If you specify a loadBalancerIP but your cloud provider does not support the feature, the loadbalancerIP field that you set is ignored.

```bash
$ kubectl expose deployment hello --port 80 --target-port 8080 --type LoadBalancer --name hello-loadbalancer
```

Note how the external IP is still showing pending.
```bash
$ kubectl get svc

NAME                 TYPE           CLUSTER-IP    EXTERNAL-IP   PORT(S)        AGE
hello                ClusterIP      10.0.89.176   <none>        8080/TCP         86m
hello-loadbalancer   LoadBalancer   10.0.250.37   <pending>     8080:30587/TCP   12s
hello-nodeport       NodePort       10.0.36.207   <none>        8080:30007/TCP   9m22s
```

In a few minutes you will be able to talk with the service without any port numbers.
```bash
$ kubectl get svc
NAME                 TYPE           CLUSTER-IP    EXTERNAL-IP    PORT(S)        AGE
hello                ClusterIP      10.0.89.176   <none>         8080/TCP         88m
hello-loadbalancer   LoadBalancer   10.0.250.37   20.71.82.154   8080:30587/TCP   90s
hello-nodeport       NodePort       10..36.207   <none>         8080:30007/TCP   10m
```
```
curl 20.71.82.154
Hello, world!
Version: 1.0.0
Hostname: web-557f59c6cf-v54ds
```
## Headless Service

Let's try out the headless service last.

```bash
$ kubectl apply -f headless.yaml
```

Open a shell inside out debug pod again and ping the headless service
```bash
$ while true; do ping -q -c1 hello-headless-service; sleep 1; done
```
Note how the ip changes every time

Let's compare the dns record
```bash
$ nslookup hello
$ nslookup hello-headless-service
```

 ## ExternalName Service

There is a last service type, that is not used to connect to pods, but for providing different DNS names for external
services. It is often much cleaner to map external services this way, an example might be different external databases for
`dev`, `qa` and `prod`.

Apply the ExternalName Service you find in `external_name.yaml`
```bash
$ kubectl apply -f external_name.yaml

service/apache created
```

This service maps the website `app.geojs.ip` to an internal DNS name. We chose this website because it doesn't use `tls`.
Open a terminal into your network-multitool container.

```
$ kubectl exec -it deploy/debug -- /bin/bash
bash-5.0#  curl geojs -v -s 2>&1 | grep Connected
% Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
Dload  Upload   Total   Spent    Left  Speed
<title>Welcome to The Apache Software Foundation!</title>-:-- --:--:--     0
100 85182  100 85182    0     0   602k      0 --:--:-- --:--:-- --:--:--  598k
```

The `app.geojs.ip` is now available with the name `geojs` in your namespace and with `geojs.YOUR_NAMESPACE.svc.cluster.local`
on the whole cluster. Try a nslookup with the shortened url.

```bash
$ nslookup geojs
```

---
 
 **Note**: 

You should always try to use FQDNs like `geojs.YOUR_NAMESPACE.svc.cluster.local`

---

## Bonus Exercise 1: Canary Deployment

Let's cleanup the services again
```bash
$ kubectl get svc -o name | xargs kubectl delete
```

The file `canary.yaml` contains two `Deployments` and one `Service` of type `ClusterIP. Start by deploying those manifests:

```bash
$ kubectl apply -f canary.yaml

deployment.apps/canary-v1 created
deployment.apps/canary-v2 created
service/canary created
```

---

**Note**:

You can use `k` if you don't want to type a little less, it is an alias for `kubectl` (`alias k=kubectl`). 

---

Look at the deployed resources using `kubectl get all`.

Why are there two `Deployments` but only one `Pod`?

Let's continuously call the `Service` `canary` to watch for changes in a separate terminal session. For this we start by spawning a pod inside the cluster.

```bash
$ k run debug --rm -it --image=praqma/network-multitool -- /bin/bash

bash-5.1#  while sleep 1; do curl canary:8080; done
```
---

Now scale up the deployment `canary-v2`:
```
$ kubectl scale deploy/canary-v2 --replicas 1
deployment.apps/canary-v2 scaled
```
To finish our smooth version change, scale down the old deployment.

```
$ kubectl scale deploy/canary-v1 --replicas 0
deployment.apps/canary-v1 scaled
```

Do you recognize some limitations with this approach to canary releases?

---
## Bonus Exercise 2: Find the error

Let's cleanup the services again

```
kubectl get svc -o name | xargs kubectl delete
```

Deploy the service contained in `faulty.yaml`. There are several errors contained in this file. Can you find them?


