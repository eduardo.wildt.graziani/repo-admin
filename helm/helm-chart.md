# Helm (1/2) Using helm charts

[[_TOC_]]

In this exercise, we will practice working with helm charts from remote repositories.

## Useful helm commands for creating and debugging helm charts

|  helm command | description   |
|---|---|
|  `helm repo add [repository-name] [url]` | Add a repository from the internet |
|  `helm repo update` | Update all repositories |
|  `helm upgrade [release] [chart] --install (-n/--namespace [namespace])` | Upgrade a release. If it does not exist on the system, install it |
|  `helm pull (--untar) [chart]` | Download and optionally unpack a helm chart |
|  `helm lint [chart] ` |  Run tests to examine a chart and identify possible issues |
|  `helm show all [chart]` |   Inspect a chart and list its default values |
|  `helm show readme [chart]` |  Show a chart's readme |
|  `helm show values [chart] ` | Display the chart’s values |
|  `helm template [release] [chart] (-f/--values [values-file]) (-s/--show-only [template file]` | Render one or all manifests of a helm chart (e.g.: `helm template test . -f values-edited.yaml -s templates/ingress.yaml`)  |
|  `helm get manifest [release]` |  display the manifest for a named release currently deployed |
|  `helm get values [release] ` |   display the values used to deploy a release currently deployed |


## Helm Chart Repositories
To get started with helm, you need to first add a chart repository:
```
$ helm repo add bitnami https://charts.bitnami.com/bitnami
"bitnami" has been added to your repositories
```

First let's verify that we installed the `bitnami` repository correctly:
```
$ helm repo list
NAME    URL                               
bitnami https://charts.bitnami.com/bitnami        
```

Update the chart repository:
```
$ helm repo update
Hang tight while we grab the latest from your chart repositories...
...Successfully got an update from the "bitnami" chart repository
```

To view the charts available in the repository `bitnami`, type;
```
helm search repo bitnami
NAME                                            CHART VERSION   APP VERSION     DESCRIPTION                                       
bitnami/bitnami-common                          0.0.9           0.0.9           DEPRECATED Chart with custom templates used in ...
bitnami/airflow                                 10.3.1          2.1.2           Apache Airflow is a platform to programmaticall...
bitnami/apache                                  8.6.3           2.4.48          Chart for Apache HTTP Server                      
bitnami/argo-cd                                 1.0.3           2.0.5           Declarative, GitOps continuous delivery tool fo...
bitnami/aspnet-core                             1.3.16          3.1.18          ASP.NET Core is an open-source framework create...
bitnami/cassandra                               8.0.3           4.0.0           Apache Cassandra is a free and open-source dist...
bitnami/cert-manager                            0.1.19          1.5.3           Cert Manager is a Kubernetes add-on to automate...
bitnami/common                                  1.8.0           1.8.0           A Library Helm Chart for grouping common logic ...
bitnami/concourse                               0.1.4           7.4.0           Concourse is a pipeline-based continuous thing-...
bitnami/dataplatform-bp1                        7.0.0           1.0.0           OCTO Data platform Kafka-Spark-Solr Helm Chart    
bitnami/dataplatform-bp2                        5.0.0           1.0.0           OCTO Data platform Kafka-Spark-Elasticsearch He...
bitnami/discourse                               4.2.12          2.7.7           A Helm chart for deploying Discourse to Kubernetes
bitnami/nginx                                   11.2.5          20200729.0.0     nginx is a standards-compliant, simple to us...
...
```


## Exploring a helm chart

As an example, we will install `nginx` the repository we just created and called `bitnami`. Before we install it, we will look at
the chart metadata, the readme and finally it's default values.

```
$ helm show chart bitnami/nginx 
annotations:
  category: Infrastructure
apiVersion: v2
appVersion: 1.21.5
dependencies:
- name: common
  repository: https://charts.bitnami.com/bitnami
  tags:
  - bitnami-common
  version: 1.x.x
```
```
$ helm show readme bitnami/nginx
```

A helm release can be configured using a `.yaml` file. The default configuration as well as often some additional documentation can be found in the `values.yaml` file.

```
$  helm show values bitnami/nginx | more
description: Chart for the nginx server
home: https://github.com/bitnami/charts/tree/master/bitnami/nginx
icon: https://bitnami.com/assets/stacks/nginx/img/nginx-stack-220x234.png
keywords:
- nginx
- http
- web
...
```

---

**Note**: 
You can exit `more` by typing `:q`

---

There are lots of configuration options. This is quite common, but most of those won't be of interest.
```
$ helm pull bitnami/nginx --untar
```

There now is a folder `./nginx`.

```bash
$ tree  nginx
nginx
├── Chart.lock
├── Chart.yaml
├── README.md
├── charts
│   └── common
│       ├── Chart.yaml
│       ├── README.md
│       ├── templates
│       │   ├── _affinities.tpl
│       │   ├── _capabilities.tpl
│       │   ├── _errors.tpl
│       │   ├── _images.tpl
│       │   ├── _ingress.tpl
│       │   ├── _labels.tpl
│       │   ├── _names.tpl
│       │   ├── _secrets.tpl
│       │   ├── _storage.tpl
│       │   ├── _tplvalues.tpl
│       │   ├── _utils.tpl
│       │   ├── _warnings.tpl
│       │   └── validations
│       │       ├── _cassandra.tpl
│       │       ├── _mariadb.tpl
│       │       ├── _mongodb.tpl
│       │       ├── _postgresql.tpl
│       │       ├── _redis.tpl
│       │       └── _validations.tpl
│       └── values.yaml
├── ci
│   ├── ct-values.yaml
│   └── values-with-ingress-metrics-and-serverblock.yaml
├── templates
│   ├── NOTES.txt
│   ├── _helpers.tpl
│   ├── deployment.yaml
│   ├── extra-list.yaml
│   ├── health-ingress.yaml
│   ├── hpa.yaml
│   ├── ingress.yaml
│   ├── ldap-daemon-secrets.yaml
│   ├── pdb.yaml
│   ├── prometheusrules.yaml
│   ├── server-block-configmap.yaml
│   ├── serviceaccount.yaml
│   ├── servicemonitor.yaml
│   ├── svc.yaml
│   └── tls-secrets.yaml
├── values.schema.json
└── values.yaml

6 directories, 43 files
```

The kubernetes resources that are generated using the Go template language are always found in `./nginx/templates`.
All `.yaml` files there will be rendered as manifests to be deployed.

## Installing the chart

Often before deploying a release, rendering its manifests can make understanding a helm chart much easier.

```bash
helm template ./nginx | more
```

Looking at the generated manifests we see, only three of the over a dozen templates where rendered. A deployment, a configmap and a service.

We will use the values from `values-dev.yaml` to customize the release.

Install the helm chart:
```
helm upgrade --install weather -f values-dev.yaml bitnami/nginx
```

View the chart's status:
```
$ helm ls
NAME            NAMESPACE               REVISION        UPDATED                                         STATUS          CHART           APP VERSION 
weather         jerry-b-anderson           1            2022-01-17 21:29:34.7139917 +0100 CET           deployed        nginx-9.7.1     1.21.5
```

Let's check, if the chart is working:

```bash
kubectl port-forward svc/weather-nginx 8080:80
```

Then `curl` or open in browser `127.0.0.1:8080`

Now let's change the value `serviceType` back to `LoadBalancer` via cli and then update the chart:
```
$ helm upgrade --install weather bitnami/nginx -f values-dev.yaml
```

Let us check if our update has worked:
```bash
NAME            NAMESPACE       REVISION        UPDATED                                 STATUS          CHART                   APP VERSION
weather         jerry-b-anderson   2            2022-01-17 21:32:13.6691706 +0100 CET   deployed        nginx-9.7.1             1.21.5
```

Next well try setting another value, this time with the `--set` command which has ultimate precedence
```
$ helm upgrade --install weather bitnami/nginx -f values-dev.yaml --set image.tag=16.2.2
```

```
$ helm ls
NAME            NAMESPACE               REVISION        UPDATED                                 STATUS          CHART           APP VERSION 
weather         jerry-b-anderson           3               2022-01-17 21:41:31.6165373 +0100 CET   deployed        nginx-9.7.1             1.21.5
```

The chart seems to be deployed, but let us check more thoroughly with ```kubectl get all```.

### Exercise 1

  1.  Can you determine the error using `kubectl describe`, `kubectl get events` or `kubectl get pod -o yaml`?
        _Spoiler_:
        ```shell
        echo VGhlIGltYWdlIG5naW54OjE2LjIuMiBkb2VzIG5vdCBleGlzdCEgKEFuZCB0aGUgY2FrZSBpcyBhIGxpZSkK | base64 -d
        ```
  2.  Use the commands `helm history weather` and `helm rollback weather <REVISION>` to undo the change. You can find the latest revision id with `helm history weather`

### Bonus Exercise 1

  1. Configure the chart to use an ingress.
  2. Configure the ingress to use tls:
     _Spoiler_:
     ```shell
     echo aW5ncmVzczoKICBlbmFibGVkOiB0cnVlCiAgcGF0aFR5cGU6IFByZWZpeAogIHRsczogdHJ1ZQogIGhvc3RuYW1lOiBjaGFuZ2VtZSEubXRhZy1rOHMtc2NodWx1bmcuc2l0ZQogIHBhdGg6IC8KICBhbm5vdGF0aW9uczoKICAgIGt1YmVybmV0ZXMuaW8vaW5ncmVzcy5jbGFzczogbmdpbngKICAgIGNlcnQtbWFuYWdlci5pby9jbHVzdGVyLWlzc3VlcjogbGV0c2VuY3J5cHQKICAgIGt1YmVybmV0ZXMuaW8vdGxzLWFjbWU6ICJ0cnVlIgo= | base64 -d
     ```
     
